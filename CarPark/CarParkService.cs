using System;
using CarPark.ChargeStrategies;

namespace CarPark
{
    internal class CarParkService : ICarParkService
    {
        private readonly ChargeStrategyFactory _chargeStrategyFactory;

        public CarParkService()
        {
            _chargeStrategyFactory = new ChargeStrategyFactory();
        }
        
        public Charge Park(ChargeMode chargeMode, DateTime timeIn, DateTime timeOut)
        {
            var strategy = _chargeStrategyFactory.Build(chargeMode);
            return strategy.Charge(timeIn, timeOut);
        }
    }
}