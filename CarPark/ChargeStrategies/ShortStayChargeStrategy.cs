using System;
using CarPark.Time;

namespace CarPark.ChargeStrategies
{
    internal class ShortStayChargeStrategy : IChargeStrategy
    {
        private readonly ITimer _timeCalculator;

        public ShortStayChargeStrategy()
        {
            _timeCalculator = new WeekdayBusinessHourTimer();
        }
        
        public Charge Charge(DateTime timeIn, DateTime timeOut)
        {
            return new Charge(1.1 * _timeCalculator.UnitsChargeable(timeIn, timeOut));
        }
    }
}