using System;

namespace CarPark.ChargeStrategies
{
    internal class ChargeStrategyFactory
    {
        public IChargeStrategy Build(ChargeMode mode)
        {
            return mode switch
            {
                ChargeMode.LongStay => new LongStageChargeStrategy(),
                ChargeMode.ShortStay => new ShortStayChargeStrategy(),
                _ => throw new ArgumentOutOfRangeException(nameof(mode), mode, null)
            };
        } 
    }
}