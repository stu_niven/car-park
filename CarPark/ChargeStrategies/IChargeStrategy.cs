using System;

namespace CarPark.ChargeStrategies
{
    internal interface IChargeStrategy
    {
        Charge Charge(DateTime timeIn, DateTime timeOut);
    }
}