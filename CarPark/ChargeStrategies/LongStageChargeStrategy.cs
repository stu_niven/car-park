using System;
using CarPark.Time;

namespace CarPark.ChargeStrategies
{
    internal class LongStageChargeStrategy : IChargeStrategy
    {
        private readonly ITimer _timeCalculator;

        public LongStageChargeStrategy()
        {
            _timeCalculator = new WholeDayInclusiveTimer();
        }
        
        public Charge Charge(DateTime timeIn, DateTime timeOut)
        {
            return new Charge(7.5 * _timeCalculator.UnitsChargeable(timeIn, timeOut));
        }
    }
}