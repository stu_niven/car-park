﻿using System;

namespace CarPark
{
    class Program
    {
        static void Main(string[] args)
        {
            var carParkService = new CarParkService();
            var charge = carParkService.Park(ChargeMode.LongStay, DateTime.Now, DateTime.Now.AddDays(3));
            Console.WriteLine($"Charge is: {charge}");
            
            // A short stay from 07/09/2017 16:50:00 to 09/09/2017 19:15:00 would cost £12.28
            var charge2 = carParkService.Park(ChargeMode.ShortStay, 
                new DateTime(2017, 09, 07, 16, 50, 0), 
                new DateTime(2017, 09, 09, 19, 15, 0));
            Console.WriteLine($"Charge 2 is: {charge2}");

            // A long stay from 07/09/2017 07:50:00 to 09/09/2017 05:20:00 would cost £22.50
            var charge3 = carParkService.Park(ChargeMode.LongStay, 
                new DateTime(2017, 09, 07, 07, 50, 0), 
                new DateTime(2017, 09, 09, 5, 20, 0));
            Console.WriteLine($"Charge 3 is: {charge3}");
        }
    }
}