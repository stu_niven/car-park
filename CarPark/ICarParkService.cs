using System;

namespace CarPark
{
    internal interface ICarParkService
    {
        Charge Park(ChargeMode chargeMode, DateTime timeIn, DateTime timeOut);
    }
}