using System;
using System.Linq;
using CarPark.ChargeStrategies;

namespace CarPark.Time
{
    internal class WeekdayBusinessHourTimer : ITimer
    {
        private const int StartHour = 8;
        private const int EndHour = 18;
        private const int HoursPerDay = EndHour - StartHour;

        public double UnitsChargeable(DateTime timeIn, DateTime timeOut)
        {
            var timeQuantity = timeOut - timeIn;
            
            if (timeQuantity < TimeSpan.FromDays(0))
                throw new NotSupportedException();

            if (timeQuantity.TotalDays < 1)
                return OneDayChargeCalc(timeIn, timeOut).TotalHours;

            return CumulativeTotalHours(timeIn, timeOut);
        }

        private static double CumulativeTotalHours(in DateTime timeIn, in DateTime timeOut)
        {
            var cumulativeHours = TimeSpan.Zero;

            cumulativeHours += AddSpareNonWeekendHours(timeIn, timeOut);
            cumulativeHours += AdjustTimeOutDay(timeOut);
            cumulativeHours += AdjustTimeInDay(timeIn);
            cumulativeHours += AddWholeWeeks(timeIn, timeOut);
            
            return cumulativeHours.TotalHours;
        }

        private static TimeSpan AddWholeWeeks(in DateTime timeIn, in DateTime timeOut)
        {
            var startOfStartDay = StartOfDay(timeIn);
            var endOfEndDay = EndOfDay(timeOut);
            var timeQuantity = endOfEndDay - startOfStartDay;
            var weeks = Math.Floor(timeQuantity.TotalDays / 7);
            return TimeSpan.FromHours(weeks * 5 * HoursPerDay);
        }

        private static TimeSpan AdjustTimeInDay(in DateTime timeIn)
        {
            if (!IsChargeDay(timeIn.DayOfWeek)) // if weekend is done in spare calc
                return TimeSpan.Zero;
            
            if (timeIn < ChargeStart(timeIn))
                return TimeSpan.Zero;
            
            var deductedDay = TimeSpan.FromHours(-HoursPerDay);
            var chargeEnd = ChargeEnd(timeIn);
            if (timeIn > chargeEnd)
                return deductedDay;
            
            return deductedDay + (chargeEnd - timeIn);
        }

        private static TimeSpan AdjustTimeOutDay(in DateTime timeOut)
        {
            if (!IsChargeDay(timeOut.DayOfWeek))
                return TimeSpan.Zero;
            
            if (timeOut > ChargeEnd(timeOut))
                return TimeSpan.Zero;
            
            var deductedDay = TimeSpan.FromHours(-HoursPerDay);
            var chargeStart = ChargeStart(timeOut);
            
            if (timeOut < chargeStart) 
                return deductedDay;

            return deductedDay + (timeOut - chargeStart);
        }

        private static TimeSpan AddSpareNonWeekendHours(in DateTime timeIn, in DateTime timeOut)
        {
            var startOfStartDay = StartOfDay(timeIn);
            var endOfEndDay = EndOfDay(timeOut);

            var totalDays = (int) (endOfEndDay - startOfStartDay).TotalDays;
            var spareDaysCount = totalDays % 7;
            
            if (spareDaysCount < 1)
                return TimeSpan.Zero;

            var startDayIndex = (int) timeIn.DayOfWeek;
            var spareDays = Enumerable.Range(startDayIndex, spareDaysCount);
            var spareDaysNotWeekends = spareDays.Select(x => x % 7).Where(y => y > 0 && y < 6);
            return TimeSpan.FromHours(spareDaysNotWeekends.Count() * HoursPerDay);
        }

        private static DateTime EndOfDay(in DateTime time)
        {
            return StartOfDay(time).AddDays(1);
        }

        private static DateTime StartOfDay(DateTime time)
        {
            return new DateTime(time.Year, time.Month, time.Day);
        }
        
        private static DateTime ChargeStart(in DateTime time)
        {
            return new DateTime(time.Year, time.Month, time.Day, StartHour, 0, 0);
        }
        
        private static DateTime ChargeEnd(in DateTime time)
        {
            return new DateTime(time.Year, time.Month, time.Day, EndHour, 0, 0);
        }

        private static TimeSpan OneDayChargeCalc(DateTime timeIn, DateTime timeOut)
        {
            if (timeOut.Hour < StartHour || timeIn.Hour >= EndHour)
                return TimeSpan.Zero;

            var chargeIn = timeIn.Hour < StartHour ? new DateTime(timeIn.Year, timeIn.Month, timeIn.Day, StartHour, 0, 0) : timeIn;
            var chargeOut = timeOut.Hour >= EndHour ? new DateTime(timeOut.Year, timeOut.Month, timeOut.Day, EndHour, 0, 0) : timeOut;

            var timeQuantity = chargeOut - chargeIn;
            return timeQuantity;
        }

        private static bool IsChargeDay(DayOfWeek inDayOfWeek)
        {
            return !(inDayOfWeek == DayOfWeek.Saturday || inDayOfWeek == DayOfWeek.Sunday);
        }
    }
}