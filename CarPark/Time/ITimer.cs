using System;

namespace CarPark.Time
{
    public interface ITimer
    {
        double UnitsChargeable(DateTime timeIn, DateTime timeOut);
    }
}