using System;

namespace CarPark.Time
{
    internal class WholeDayInclusiveTimer : ITimer
    {
        public double UnitsChargeable(DateTime timeIn, DateTime timeOut)
        {
            var timeQuantity = timeOut - timeIn;
            if (timeQuantity < TimeSpan.FromDays(0))
                throw new NotSupportedException();
            
            if (timeQuantity < TimeSpan.FromDays(1))
                return 1;
            
            var baseDay = new DateTime(timeIn.Year, timeIn.Month, timeIn.Day);
            var baseEndDay = new DateTime(timeOut.Year, timeOut.Month, timeOut.Day).AddDays(1);
            
            return (int) (baseEndDay - baseDay).TotalDays;
        }
    }
}