namespace CarPark
{
    public class Charge
    {
        private readonly double _amount;

        public Charge(double amount)
        {
            _amount = amount;
        }

        public override string ToString()
        {
            return $"{_amount:.##}";
        }
    }
}