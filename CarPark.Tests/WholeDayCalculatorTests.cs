using System;
using CarPark.Time;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarPark.Tests
{
    [TestClass]
    public class WholeDayInclusiveTimerTests
    {
        [TestMethod]
        public void ReturnsOneForSameDayDeparture()
        {
            var timer = new WholeDayInclusiveTimer();
            var days = timer.UnitsChargeable(new DateTime(2020, 1, 1), 
                new DateTime(2020, 1,1));
            Assert.AreEqual(1, days);
        }
        
        [TestMethod]
        public void ReturnsTwoForNextDayDeparture()
        {
            var timer = new WholeDayInclusiveTimer();
            var days = timer.UnitsChargeable(new DateTime(2020, 6, 1), 
                new DateTime(2020, 6, 2));
            
            Assert.AreEqual(2, days);
        }
        
        [TestMethod]
        public void ReturnsNineForNineDayDeparture()
        {
            var timer = new WholeDayInclusiveTimer();
            var days = timer.UnitsChargeable(new DateTime(2020, 6, 1), 
                new DateTime(2020, 6, 9));
            Assert.AreEqual(9, days);
        }
        
        [TestMethod]
        public void Returns3DaysForExample()
        {
            var timer = new WholeDayInclusiveTimer();
            var days = timer.UnitsChargeable(new DateTime(2017, 09, 07, 07, 50, 0), 
                new DateTime(2017, 09, 09, 5, 20, 0));
            Assert.AreEqual(3, days);
        }
        
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void ThrowsNotSupportedForTimeOutLessThanTimeIn()
        {
            var timer = new WholeDayInclusiveTimer();
            timer.UnitsChargeable(new DateTime(2020, 1, 1), 
                new DateTime(2019, 1,1));
        }
    }
}