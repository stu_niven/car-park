using System;
using CarPark.Time;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarPark.Tests
{
    [TestClass]
    public class WeekdayBusinessHourTimerTests
    {
        [TestMethod]
        public void Returns00ForSameTimeDeparture()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 12, 0, 0);
            var timeOut = new DateTime(2020, 1, 1, 12, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(0, hours);
        }
        
        [TestMethod]
        public void Returns00ForWeekend()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 4, 12, 0, 0);
            var timeOut = new DateTime(2020, 1, 5, 12, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(0, hours);
        }
        
        [TestMethod]
        public void Returns01ForCrossHour()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 11, 30, 0);
            var timeOut = new DateTime(2020, 1, 1, 12, 30, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut); 
            Assert.AreEqual(1, hours);
        }
        
        [TestMethod]
        public void Returns00Point5ForSameHourDeparture()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 12, 0, 0);
            var timeOut = new DateTime(2020, 1, 1, 12, 30, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(0.5, hours);
        }
        
        [TestMethod]
        public void Returns03ForMiddayArrivalAndDeparture()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 12, 0, 0);
            var timeOut = new DateTime(2020, 1, 1, 15, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(3, hours);
        }
        
        [TestMethod]
        public void Returns09ForPreChargeArrivalAndDepartureMidday()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 4, 30, 0);
            var timeOut = new DateTime(2020, 1, 1, 16, 30, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(8.5, hours);
        }
        
        [TestMethod]
        public void Returns09ForMiddayArrivalAndDepartureAfterCharge()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 9, 0, 0);
            var timeOut = new DateTime(2020, 1, 1, 23, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(9, hours);
        }
        
        [TestMethod]
        public void Returns10ForOneWholeDay()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 2, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(10, hours);
        }
        
        [TestMethod]
        public void Returns10ForOverWeekend()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 3, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 5, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(10, hours);
        }
        
        [TestMethod]
        public void Returns10ForAfterWeekend()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 4, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 7, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(10, hours);
        }
        
        [TestMethod]
        public void Returns20ForTwoWholeDays()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 2, 23, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(20, hours);
        }
        
        [TestMethod]
        public void Returns20ForOverWeekend()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 3, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 7, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(20, hours);
        }
        
        [TestMethod]
        public void Returns30ForThreeWholeDays()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 3, 23, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(30, hours);
        }
        
        [TestMethod]
        public void Returns50ForOneWholeWeek()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 8, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(50, hours);
        }
        
        [TestMethod]
        public void Returns60ForOneWholeWeekAndADay()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 9, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(60, hours);
        }
        
        [TestMethod]
        public void Returns50ForOneWholeWeekMidweek()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 4, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 11, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(50, hours);
        }
        
        [TestMethod]
        public void Returns200ForFourWholeWeekMidweek()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 1, 0, 0, 0);
            var timeOut = new DateTime(2020, 1, 29, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(200, hours);
        }
        
        [TestMethod]
        public void Returns200ForFourWholeWeekStartWeek()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2020, 1, 6, 0, 0, 0);
            var timeOut = new DateTime(2020, 2, 3, 0, 0, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual(200, hours);
        }
        
        [TestMethod]
        public void Returns11Point16ForExample()
        {
            var timer = new WeekdayBusinessHourTimer();
            var timeIn = new DateTime(2017, 09, 07, 16, 50, 0);
            var timeOut = new DateTime(2017, 09, 09, 19, 15, 0);
            
            var hours = timer.UnitsChargeable(timeIn, timeOut);
            Assert.AreEqual("11.17", $"{hours:.##}");
        }
        
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void ThrowsNotSupportedForTimeOutLessThanTimeIn()
        {
            var timer = new WeekdayBusinessHourTimer();
            timer.UnitsChargeable(new DateTime(2020, 1, 1), 
                new DateTime(2019, 1,1));
        }
    }
}